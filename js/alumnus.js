//  Import Student
const Student = require("./student.js");

//Khai báo class alumnus
class Alumnus extends Student {
    constructor(fullname, birthday, country, schoolname, classname, studentphone, majors, studentcode) {
        super(fullname, birthday, country, schoolname, classname, studentphone);
        this.majors = majors;
        this.studentcode = studentcode;
    }
    getAlumnus() {
        return {
            "Họ tên": this.fullname,
            "Ngày sinh": this.birthday,
            "Quê quán": this.country,
            "Tên trường": this.schoolname,
            "Lớp": this.classname,
            "Số điện thoại": this.studentphone,
            "Chuyên ngành": this.majors,
            "Mã số SV": this.studentcode
        }
    }
}

//Export Alumnus
module.exports = Alumnus;