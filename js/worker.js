// Import User
const User = require('./user.js');

//Khai báo class
class Worker extends User {
    constructor(fullname, birthday, country, job, workplace, salary) {
        super(fullname, birthday, country);
        this.job = job;
        this.workplace = workplace;
        this.salary = salary;
    }
    getWorker() {
        return {
            "Họ tên": this.fullname,
            "Ngày sinh": this.birthday,
            "Quê quán": this.country,
            "Ngành nghề": this.job,
            "Nơi làm việc": this.workplace,
            "Lương": this.salary
        }
    }
}

// Export Student
module.exports = Worker;