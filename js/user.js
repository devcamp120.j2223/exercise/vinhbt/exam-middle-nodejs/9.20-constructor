class User {
    constructor(fullname, birthday, country) {
        this.fullname = fullname;
        this.birthday = birthday;
        this.country = country;
    }
    getUser() {
        return {
            "Họ tên": this.fullname,
            "Ngày sinh": this.birthday,
            "Quê quán": this.country
        }
    }
}
module.exports = User;