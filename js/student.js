// Import User
const User = require('./user.js')

//Khai báo class
class Student extends User {
    constructor(fullname, birthday, country, schoolname, classname, studentphone) {
        super(fullname, birthday, country);
        this.schoolname = schoolname;
        this.classname = classname;
        this.studentphone = studentphone;
    }
    getStudent() {
        return {
            "Họ tên": this.fullname,
            "Ngày sinh": this.birthday,
            "Quê quán": this.country,
            "Tên trường": this.schoolname,
            "Lớp": this.classname,
            "Số điện thoại": this.studentphone
        }
    }
}

// Export Student
module.exports = Student;