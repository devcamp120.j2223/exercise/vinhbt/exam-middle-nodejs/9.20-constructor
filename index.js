// IMPORT CÁC CLASS
const User = require('./js/user.js');
const Worker = require('./js/worker.js');
const Student = require('./js/student.js');
const Alumnus = require('./js/alumnus.js');


//KHAI BÁO CỔNG EXPRESS
const express = require('express');


//  KHỞI TẠO APP EXPRESS
const app = express();


// KHAI BÁO CỔNG NODEJS
const port = 8000;


// Khai báo biến User
var user = new User('vinhbt', '18/09/1994', 'SG');
console.log(user.getUser())

// Khai báo biến Worker
var worker = new Worker('vinhbt', '18/09/1994', 'SG', 'QC Dept', 'TPT Industry', '600$');
console.log(worker.getWorker());

// Khai báo biến Student
var student = new Student('vinhbt', '18/09/1994', 'SG', 'Hutech', 'DDH', '0777682345');
console.log(student.getStudent());

// Khai báo biến Alumnus
var alumnus = new Alumnus('vinhbt', '18/09/1994', 'SG', 'Hutech', 'DDH', '0777682345', 'Ky su', '21809');
console.log(alumnus.getAlumnus());


//  KHAI BÁO API

//GET USER
app.get('/users', (request, response) => {
    response.status(200).json({
        Users: user
    })
});

//GET WORKER
app.get('/workers', (request, response) => {
    response.status(200).json({
        Workers: worker
    })
});

//GET STUDENT
app.get('/students', (request, response) => {
    response.status(200).json({
        Students: student
    })
});

// GET ALUMNUS
app.get('/alumnus', (request, response) => {
    response.status(200).json({
        Alumnus: alumnus
    })
});


//CHẠY APP EXPRESS
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});
